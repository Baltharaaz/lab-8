Tasks to answer in your own README.md that you submit on Canvas:

1.  See logger.log, why is it different from the log to console?

		Answer: The logger accepts a configuration for reporting information from logger.properties. It follows that config and reports more than is told by the console.
		
1.  Where does this line come from? FINER org.junit.jupiter.engine.execution.ConditionEvaluator logResult Evaluation of condition [org.junit.jupiter.engine.extension.DisabledCondition] resulted in: ConditionEvaluationResult [enabled = true, reason = '@Disabled is not present']

		Answer: It is coming from the logger within JUnit (specifically, the condition evaluator within JUnit), notifying the user of the state of any test (enabled or disabled).
		
1.  What does Assertions.assertThrows do?

		Answer: Asserts that the specified executable throws an exception of the specified type. Method fails if no exception or a exception other than the specified is thrown.
		
1.  See TimerException and there are 3 questions

    1.  What is serialVersionUID and why do we need it? (please read on Internet)
	
			Answer: Serialization runtime associates each serializable class with a serialVersionUID, which serves as a version number. It is used during desearialization to verify that the sender and receiver of a serialized object have loaded classes for that object that are compatible with respect to serialization. If these serialVersionUIDs do not match, desearialization throws an InvalidClassException.
			
    2.  Why do we need to override constructors?
	
			Answer: Constructors must be "overwritten" because we may need to send certain parameters up the hierarchy, something impossible if we do not "overwrite" the constructors.
			
    3.  Why we did not override other Exception methods?
	
			Answer: It is not necessary for the functionality of the TimerException, since they work identically to where they are inherited from (using the same information). The constructors require overwriting because otherwise we are not able to pass parameters to the superconstructors.
			
1.  The Timer.java has a static block static {}, what does it do? (determine when called by debugger)

		Answer: This static block is running at the start of the test for the Timer to acquire the configuration and, using that config, configure the logger. It then sends the first line of starting the app to the logger.
		
1.  What is README.md file format how is it related to bitbucket? (https://confluence.atlassian.com/bitbucketserver/markdown-syntax-guide-776639995.html)

		Answer: The format of README.md is Markdown syntax. Bitbucket uses the syntax to format pull requests and comments, as well as automatically display README.md since it is in Markdown syntax.
		
1.  Why is the test failing? what do we need to change in Timer? (fix that all tests pass and describe the issue)

		Answer: The exception thrown by the test is something other than the expected type of TimerException (it is throwing a NullPointerException). The issue is caused because the finally block executes with timeNow still equal to null, since the finally block always executes when the try block exits, whether through failure or success. In the Timer, we must move the check of the timeToWait outside the try to avoid the finally block, or alter the finally block to avoid the access of timeNow while it is null, or move the assignment of timeNow to above the check so that the access of timeNow in finally is not null.
		
1.  What is the actual issue here, what is the sequence of Exceptions and handlers (debug)

		Answer: As said in 7, the TimerException is throw, leaving the try block. But the finally block is ALWAYS executed upon exit (any form of exit) from the try block, so when the finally block attempts to access and print the timeNow while it is still null, it throws a NullPointerException.
		
1.  Make a printScreen of your eclipse JUnit5 plugin run (JUnit window at the bottom panel) 

1.  Make a printScreen of your eclipse Maven test run, with console

1.  What category of Exceptions is TimerException and what is NullPointerException

		Answer: NullPointerException is a built-in exception. Specifically, a RuntimeException. TimerException is a user-defined compile-time exception.
		
1.  Push the updated/fixed source code to your own repository.

		URL: https://bitbucket.org/Baltharaaz/lab-8/src/master/